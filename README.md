# README #

This project is the firmware code for the LiPo low-voltage cutoff project.

Full project description on [hackster.io][]
[hackster.io]: https://www.hackster.io/jmjulien/lipo-low-voltage-cutoff-729ae8

### What is this repository for? ###

At the heart of the project is an aduino pro mini.
This repository hosts the arduino firmware code for this project.

### Who do I talk to? ###

Jean-Michel Julien
<jean-michel.julien@cadransys.com>