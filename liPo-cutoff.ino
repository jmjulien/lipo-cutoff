// GPIO pins definitions
const int relayOn = 3;     // Relay set on pin
const int relayOff = 2;     // Relay reset off pin
const int switchSense = 4;     // Sensor for the on pushbutton
const int powerSense = 5;     // Sensor for the state of the circuit
const int ledRed = 10;     // Red led pin
const int ledYellow = 8;     // Yellow led pin
const int ledGreen = 9;     // Green led pin
const int voltageSense = A0;     // voltage measurement pin

// ADC values for specifics voltages
const int voltageHigh = 835;  // 12V
const int voltageMed = 690;   // 10V
const int voltageLow = 615;   // 9V
const int voltageOnTreshold = 690; // 10V
const int voltageOffTreshold = 540;  // 8V under load should go back over 9 without load

// timer to read voltage sporadically
const unsigned long voltageReadDelayMs = 1000;
unsigned long voltageReadDelayStart;
int voltage;

// timer values for blinking red light in xlow voltage
const unsigned long blinkDelayMs = 250;
unsigned long blinkDelayStart;
bool blinker = false;

// the possible states of the lights
enum lightState {off, xlow, low, medium, high};

// Read the voltage
int readVoltage()
{
  analogRead(voltageSense); //stabilise

  int temp = analogRead(voltageSense);
  delay(50);
  temp += analogRead(voltageSense);
  delay(50);
  temp += analogRead(voltageSense);

  return temp/3;
}

// Set the lights color based on a voltage
void evaluateLightsColor(int voltage)
{
  if (voltage >= voltageHigh)
  {
    setLightsColors(lightState::high);
  }
  else if (voltage >= voltageMed)
  {
    setLightsColors(lightState::medium);
  }
  else if (voltage >= voltageLow)
  {
    setLightsColors(lightState::low);
  }
  else
  {
    setLightsColors(lightState::xlow);
  }
}

// Set the lights color based on a light state
void setLightsColors(lightState state)
{
  switch (state)
  {
    case lightState::high:
      digitalWrite(ledRed, HIGH);
      digitalWrite(ledYellow, HIGH);
      digitalWrite(ledGreen, LOW);
      break;
    case lightState::medium:
      digitalWrite(ledRed, HIGH);
      digitalWrite(ledGreen, HIGH);
      digitalWrite(ledYellow, LOW);
      break;
    case lightState::low:
      digitalWrite(ledYellow, HIGH);
      digitalWrite(ledGreen, HIGH);
      digitalWrite(ledRed, LOW);
      break;
    case lightState::xlow:
      digitalWrite(ledYellow, HIGH);
      digitalWrite(ledGreen, HIGH);
      digitalWrite(ledRed, blinker?LOW:HIGH);
      break;
    default:
      digitalWrite(ledRed, HIGH);
      digitalWrite(ledYellow, HIGH);
      digitalWrite(ledGreen, HIGH);
      break;
  }
}

// turn on the relay
void turnOn()
{
  digitalWrite(relayOn, HIGH);
  delay(1000);
  digitalWrite(relayOn, LOW);
}

// turn off the relay
void turnOff()
{
  digitalWrite(relayOff, HIGH);
  // We probably won't pass here ;-)
  delay(1000);
  digitalWrite(relayOff, LOW);  
}

// Initialize all the GPIO to be in a correct initial state
void setup() {
  pinMode(switchSense, INPUT);
  pinMode(powerSense, INPUT);

  pinMode(ledRed, OUTPUT);
  digitalWrite(ledRed, HIGH);
  pinMode(ledYellow, OUTPUT);
  digitalWrite(ledYellow, HIGH);
  pinMode(ledGreen, OUTPUT);
  digitalWrite(ledGreen, HIGH);

  pinMode(relayOn, OUTPUT);
  digitalWrite(relayOn, LOW);
  pinMode(relayOff, OUTPUT);
  digitalWrite(relayOff, LOW);

  blinkDelayStart = millis();
  voltage = readVoltage(); //So we have a valid initial value on the first loop iteration
}

// the meat of it all
void loop() {

  // reverse blinker state every blinkDelay
  if ((millis() - blinkDelayStart) >= blinkDelayMs)
  {
    blinker = !blinker;
    blinkDelayStart = millis(); //reset the timer
  }

  bool pbSwitch = (digitalRead(switchSense) == HIGH); // is the on switch pressed
  bool turnedON = (digitalRead(powerSense) == HIGH);  // is the circuit activated

  //voltage read periodically
  if ((millis() - voltageReadDelayStart) >= voltageReadDelayMs)
  {
    voltage = readVoltage();
    
    if (voltage <= voltageOffTreshold && turnedON)  // turn off if voltage too low and the circuit is on
    {
      turnOff();
    }
    
    voltageReadDelayStart = millis(); //reset the voltage read timer
  }

  if (pbSwitch)  // is the on switch pressed ?
  {
    // display voltage level on the lights
    evaluateLightsColor(voltage);

    if (!turnedON) // if the circuit is not activated
    {
      if (voltage >= voltageOnTreshold) // is the voltage high enough
      {
        turnOn();  // activate the circuit
      }
    }
  }
  else
  {
    // if the on switch is not pressed, the lights should be off
    setLightsColors(lightState::off);
  }
}
